﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VTYS
{
    public partial class Form1 : Form
    {
        public static string[,] kullaniciAlanlari = new string[1,3];
        public static string kadi,yetki;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[,] haberler = new string[5, 2];
            int i = 0;
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = "SELECT TOP 4 yazarId, haberIcerik FROM Haberler";
            SqlCommand cmd = new SqlCommand(sql, conn);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                haberler[i,0] = dt.Rows[i][0].ToString();
                haberler[i, 1] = dt.Rows[i][1].ToString();
                i = i + 1;
            }
            conn.Close();

            string baglantiCumlesi2 = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn2 = new SqlConnection(baglantiCumlesi2);
            if (conn2.State == ConnectionState.Closed)
            {
                conn2.Open();
            }
            string[] yazarlar = new string[4];
            string sql2 = "SELECT Kisiler.id, Kisiler.kullaniciAdi FROM Kisiler INNER JOIN Yonetici ON Kisiler.id=Yonetici.id";
            SqlCommand cmd2 = new SqlCommand(sql2, conn2);
            DataTable dt2 = new DataTable();
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            da2.Fill(dt2);
            for (int k = 0; k < 4; k++)
            {
                for (int l = 0; l < dt2.Rows.Count; l++)
                {
                    if (dt2.Rows[l][0].ToString().Equals(haberler[k,0]))
                    {
                        yazarlar[k] = dt2.Rows[l][1].ToString();
                        break;
                    }
                }
            }
            conn2.Close();


            for (int j = 0; j < 4; j++)
            {
                listBox1.Items.Add("* "+haberler[j,1].ToString());
                listBox1.Items.Add("via " + yazarlar[j]);
                listBox1.Items.Add(" ");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            string sql = "SELECT * FROM Kisiler WHERE kullaniciAdi =@kadi AND sifre=@sifre";
            SqlParameter prms1 = new SqlParameter("@kadi", tbKadi.Text);
            SqlParameter prms2 = new SqlParameter("@sifre", tbSifre.Text);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            cmd.Parameters.Add(prms2);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                kadi = dt.Rows[0][0].ToString();
                yetki = dt.Rows[0][7].ToString();
                Mailler mailEkrani = new Mailler();
                mailEkrani.Show();
                conn.Close();
                this.Visible = false;
            }
            else
            {
                MessageBox.Show("Hatali Giris!");
            }

            conn.Close();
        }

        private void bKayitEkrani_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Kayit kayitEkrani = new Kayit();
            kayitEkrani.Show();           
        }

        private void label7_Click(object sender, EventArgs e)
        {
            if (tbKadi.Text == "")
            {
                MessageBox.Show("Lütfen kullanıcı adınızı girin!");
            }
            else
            {
                string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
                SqlConnection conn = new SqlConnection(baglantiCumlesi);
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                string sql = "SELECT id,gizliSoru,cevap FROM Kisiler WHERE kullaniciAdi =@kadi";
                SqlParameter prms1 = new SqlParameter("@kadi", tbKadi.Text);
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.Add(prms1);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count != 0)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        kullaniciAlanlari[0, i] = dt.Rows[0][i].ToString();
                    }
                    conn.Close();
                    this.Visible = false;
                    GizliSoru sifreDegisimi = new GizliSoru();
                    sifreDegisimi.Show();
                }          
                else
                {
                    MessageBox.Show("Böyle bir kullanıcı adı mevcut değil!");
                }
            }           
            
        }
    }
}
