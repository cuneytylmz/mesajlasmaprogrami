﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VTYS
{
    public partial class Mailler : Form
    {
        public Mailler()
        {
            InitializeComponent();
        }


        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        DataTable dt3 = new DataTable();
        DataTable dt4 = new DataTable();
        bool silinenKutusu = false;
        string[,] kisiler = new string[100,2];

        public void IDkontrol() {
            if (DataGridView1.Columns[0].Name.ToString().Equals("ID"))
            {
                DataGridView1.Columns.Remove("ID");

            }
        }

        public void alanKontrol() {
            if (DataGridView1.Columns.Count == 7)
            {
                DataGridView1.Columns[6].Visible = false;
            }
        }

        public void gelen() {
            dt.Clear();
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            string sql = "SELECT Mesajlar.id as ID, Mesajlar.iletiSec as İletiSeç, Kisiler.kullaniciAdi as Gönderen, Mesajlar.baslik as Başlık, Mesajlar.mesaj as Mesaj, Mesajlar.isOkundu as Okundu, Mesajlar.olusturulmaTarihi as Tarih FROM Mesajlar INNER JOIN Kisiler ON Mesajlar.gonderenId = Kisiler.id WHERE Mesajlar.aliciId =@alici AND Mesajlar.isSilindi = @silindi AND Mesajlar.isTaslak = @taslak AND Mesajlar.isGereksiz = @gereksiz";
            SqlParameter prms1 = new SqlParameter("@alici", Form1.kadi);
            SqlParameter prms2 = new SqlParameter("@silindi", false);
            SqlParameter prms3 = new SqlParameter("@taslak", false);
            SqlParameter prms4 = new SqlParameter("@gereksiz", false);

            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            cmd.Parameters.Add(prms2);
            cmd.Parameters.Add(prms3);
            cmd.Parameters.Add(prms4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            DataGridView1.DataSource = dt;
            DataGridView1.Columns[4].Visible = true;
            conn.Close();
        }

        public void gizle() {
            Label1.Visible = true;
            Label2.Visible = true;
            Label3.Visible = true;
            ComboBox1.Visible = true;
            txtbaslik.Visible = true;
            txtmesaj.Visible = true;
            btngonder.Visible = true;
            btnkaydet.Visible = true;
            DataGridView1.Visible = false;
            btnsil.Visible = false;
            btnokundu.Visible = false;
        }

        public void goster()
        {
            Label1.Visible = false;
            Label2.Visible = false;
            Label3.Visible = false;
            ComboBox1.Visible = false;
            txtbaslik.Visible = false;
            txtmesaj.Visible = false;
            btngonder.Visible = false;
            btnkaydet.Visible = false;
            DataGridView1.Visible = true;
            btnsil.Visible = true;
            btnokundu.Visible = true;
        }

        private void Mailler_Load(object sender, EventArgs e)
        {
            this.Visible = true;
            gelen();
            IDkontrol();
            if (Form1.yetki == "Y")
            {
                btnpanel.Visible = true;
            }
        }

        private void btnyeni_Click(object sender, EventArgs e)
        {
            txtbaslik.Text = String.Empty;
            txtmesaj.Text = String.Empty;
            ComboBox1.Items.Clear();
            gizle();
            dt2.Clear();
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = "SELECT id, kullaniciAdi, kisilerType FROM Kisiler WHERE NOT id=@id";
            SqlParameter prms1 = new SqlParameter("@id", Form1.kadi);           
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt2);
            SqlDataReader dr = cmd.ExecuteReader();
            int i = 0;
            int j = 0;
            while (dr.Read())
            {
                if (Form1.yetki == "K")
                {
                    if (dt2.Rows[i][2].ToString().Equals("K"))
                    {
                        kisiler[j,0] = dt2.Rows[i][0].ToString();
                        kisiler[j, 1] = dt2.Rows[i][1].ToString();
                        j++;
                        ComboBox1.Items.Add(dt2.Rows[i][1].ToString());
                    }
                }
                else if (Form1.yetki == "Y")
                {
                    kisiler[j, 0] = dt2.Rows[i][0].ToString();
                    kisiler[j, 1] = dt2.Rows[i][1].ToString();
                    j++;
                    ComboBox1.Items.Add(dt2.Rows[i][1].ToString());
                }
                i = i + 1;
            }
            conn.Close();
        }

        private void btngelen_Click(object sender, EventArgs e)
        {
            goster();
            gelen();
            IDkontrol();
            alanKontrol();
            DataGridView1.Columns[1].Visible = true;
        }

        private void btnokundu_Click(object sender, EventArgs e)
        {
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql;
            SqlParameter prms1,prms2;
            SqlCommand cmd;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["Okundu"].Equals(true))
                {
                    sql = "UPDATE Mesajlar SET isOkundu=@okundu WHERE id=@id";
                    prms1 = new SqlParameter("@okundu", true);
                    prms2 = new SqlParameter("@id", dt.Rows[i][0]);
                    cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.Add(prms1);
                    cmd.Parameters.Add(prms2);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    sql = "UPDATE Mesajlar SET isOkundu=@okundu WHERE id=@id";
                    prms1 = new SqlParameter("@okundu", false);
                    prms2 = new SqlParameter("@id", dt.Rows[i][0]);
                    cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.Add(prms1);
                    cmd.Parameters.Add(prms2);
                    cmd.ExecuteNonQuery();
                }
            }
            conn.Close();
            MessageBox.Show("Kayıt(lar) Güncellendi!");
        }

        private void btnsil_Click(object sender, EventArgs e)
        {
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql;
            SqlParameter prms1, prms2;
            SqlCommand cmd;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (silinenKutusu == false)
                {
                    if (dt.Rows[i]["İletiSeç"].Equals(true))
                    {
                        sql = "UPDATE Mesajlar SET isSilindi=@silindi WHERE id=@id";
                        prms1 = new SqlParameter("@silindi", true);
                        prms2 = new SqlParameter("@id", dt.Rows[i][0]);
                        cmd = new SqlCommand(sql, conn);
                        cmd.Parameters.Add(prms1);
                        cmd.Parameters.Add(prms2);
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        sql = "UPDATE Mesajlar SET isSilindi=@silindi WHERE id=@id";
                        prms1 = new SqlParameter("@silindi", false);
                        prms2 = new SqlParameter("@id", dt.Rows[i][0]);
                        cmd = new SqlCommand(sql, conn);
                        cmd.Parameters.Add(prms1);
                        cmd.Parameters.Add(prms2);
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    sql = "DELETE FROM Mesajlar WHERE id=@id";
                    prms1 = new SqlParameter("@id", dt.Rows[i][0]);
                    cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.Add(prms1);
                    cmd.ExecuteNonQuery();
                }               
            }
            conn.Close();
            MessageBox.Show("Kayıt(lar) Silindi!");
        }

        private void btngereksiz_Click(object sender, EventArgs e)
        {
            goster();
            dt.Clear();
            DataGridView1.Columns[4].Visible = true;
            if (DataGridView1.ColumnCount == 4)
            {
                DataGridView1.Columns.Add("Column", "Okundu");
            }
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = "SELECT Mesajlar.id as ID, Mesajlar.iletiSec as İletiSeç, Kisiler.kullaniciAdi as Gönderen, Mesajlar.baslik as Başlık, Mesajlar.mesaj as Mesaj, Mesajlar.isOkundu as Okundu, Mesajlar.olusturulmaTarihi as Tarih FROM Mesajlar INNER JOIN Kisiler ON Mesajlar.gonderenId = Kisiler.id WHERE Mesajlar.aliciId =@alici AND Mesajlar.isGereksiz = @gereksiz";
            SqlParameter prms1 = new SqlParameter("@alici", Form1.kadi);
            SqlParameter prms2 = new SqlParameter("@gereksiz", true);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            cmd.Parameters.Add(prms2);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            DataGridView1.DataSource = dt;
            conn.Close();
            IDkontrol();
            alanKontrol();
            DataGridView1.Columns[1].Visible = true;
        }

        private void btntaslaklar_Click(object sender, EventArgs e)
        {
            goster();
            dt.Clear();
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = "SELECT Mesajlar.id as ID, Mesajlar.iletiSec as İletiSeç, Kisiler.kullaniciAdi as Gönderilen, Mesajlar.baslik as Başlık, Mesajlar.mesaj as Mesaj, Mesajlar.isOkundu as Okundu, Mesajlar.olusturulmaTarihi as Tarih FROM Mesajlar INNER JOIN Kisiler ON Mesajlar.aliciId = Kisiler.id WHERE Mesajlar.gonderenId =@gonderen AND Mesajlar.isTaslak = @taslaklar";
            SqlParameter prms1 = new SqlParameter("@gonderen", Form1.kadi);
            SqlParameter prms2 = new SqlParameter("@taslaklar", true);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            cmd.Parameters.Add(prms2);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            DataGridView1.DataSource = dt;
            conn.Close();
            IDkontrol();
            DataGridView1.Columns[4].Visible = false;
            DataGridView1.Columns[1].Visible = false;
            DataGridView1.Columns[6].Visible = true;
        }

        private void btngonderilen_Click(object sender, EventArgs e)
        {
            goster();
            dt.Clear();
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = "SELECT Mesajlar.id as ID, Mesajlar.iletiSec as İletiSeç, Kisiler.kullaniciAdi as Gönderilen, Mesajlar.baslik as Başlık, Mesajlar.mesaj as Mesaj, Mesajlar.isOkundu as Okundu, Mesajlar.olusturulmaTarihi as Tarih FROM Mesajlar INNER JOIN Kisiler ON Kisiler.id = Mesajlar.aliciId WHERE Mesajlar.gonderenId =@gonderen AND Mesajlar.isTaslak = @taslak";
            SqlParameter prms1 = new SqlParameter("@gonderen", Form1.kadi);
            SqlParameter prms2 = new SqlParameter("@taslak", false);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            cmd.Parameters.Add(prms2);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            DataGridView1.DataSource = dt;
            conn.Close();
            IDkontrol();
            DataGridView1.Columns[4].Visible = false;
            DataGridView1.Columns[1].Visible = false;
            DataGridView1.Columns[6].Visible = true;
        }

        private void btnsilinmis_Click(object sender, EventArgs e)
        {
            goster();
            dt.Clear();
            DataGridView1.Columns[4].Visible = true;
            if (DataGridView1.ColumnCount == 4)
            {
                DataGridView1.Columns.Add("Column", "Okundu");
            }
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = "SELECT Mesajlar.id as ID, Mesajlar.iletiSec as İletiSeç, Kisiler.kullaniciAdi as Gönderen, Mesajlar.baslik as Başlık, Mesajlar.mesaj as Mesaj, Mesajlar.isOkundu as Okundu, Mesajlar.olusturulmaTarihi as Tarih FROM Mesajlar INNER JOIN Kisiler ON Mesajlar.gonderenId = Kisiler.id WHERE Mesajlar.aliciId =@alici AND Mesajlar.isSilindi = @silindi";
            SqlParameter prms1 = new SqlParameter("@alici", Form1.kadi);
            SqlParameter prms2 = new SqlParameter("@silindi", true);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            cmd.Parameters.Add(prms2);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            DataGridView1.DataSource = dt;
            conn.Close();
            IDkontrol();
            alanKontrol();
            DataGridView1.Columns[1].Visible = true;
            silinenKutusu = true;
        }

        private void btnkaydet_Click(object sender, EventArgs e)
        {
            int? gonderilen=null;
            if (ComboBox1.Text == "Açılır Kutudan Kime Yollayacağınızı Seçin")
            {
                MessageBox.Show("Lütfen açılır kutudan kime yollayacağınızı seçin!");
                return;
            }
            else
            {
                for (int i = 0; i < kisiler.Length; i++)
                {
                    if (kisiler[i, 1].Equals(ComboBox1.Text))
                    {
                        gonderilen = int.Parse(kisiler[i, 0]);
                    }
                    
                }
            }
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = "INSERT INTO Mesajlar(iletiSec, gonderenId, aliciId, baslik, mesaj, isOkundu, isSilindi, isGereksiz, isTaslak, olusturulmaTarihi) VALUES(@iletiSec, @gonderen, @alici, @baslik, @mesaj, @okundu, @silindi, @gereksiz, @taslak, @tarih)";
            SqlParameter prms1 = new SqlParameter("@iletiSec", false);
            SqlParameter prms2 = new SqlParameter("@gonderen", Form1.kadi);
            SqlParameter prms3 = new SqlParameter("@alici", gonderilen);
            SqlParameter prms4 = new SqlParameter("@baslik", txtbaslik.Text);
            SqlParameter prms5 = new SqlParameter("@mesaj", txtmesaj.Text);
            SqlParameter prms6 = new SqlParameter("@okundu", false);
            SqlParameter prms7 = new SqlParameter("@silindi", false);
            SqlParameter prms8 = new SqlParameter("@gereksiz", false);
            SqlParameter prms9 = new SqlParameter("@taslak", true);
            SqlParameter prms10 = new SqlParameter("@tarih", DateTime.Now.Date);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            cmd.Parameters.Add(prms2);
            cmd.Parameters.Add(prms3);
            cmd.Parameters.Add(prms4);
            cmd.Parameters.Add(prms5);
            cmd.Parameters.Add(prms6);
            cmd.Parameters.Add(prms7);
            cmd.Parameters.Add(prms8);
            cmd.Parameters.Add(prms9);
            cmd.Parameters.Add(prms10);
            cmd.ExecuteNonQuery();
            txtbaslik.Text = String.Empty;
            txtmesaj.Text = String.Empty;
            ComboBox1.Text = "Açılır Kutudan Kime Yollayacağınızı Seçin";
            MessageBox.Show("Mesaj taslaklara kaydedildi!");
        }

        private void btngonder_Click(object sender, EventArgs e)
        {
            bool engel = false;

            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            int? gonderilen = null;
            if (ComboBox1.Text == "Açılır Kutudan Kime Yollayacağınızı Seçin")
            {
                MessageBox.Show("Lütfen açılır kutudan kime yollayacağınızı seçin!");
                return;
            }
            else if (ComboBox1.Text == "Destek")
            {
                gonderilen = 0;
            }
            else
            {
                for (int i = 0; i < kisiler.Length; i++)
                {
                    if (kisiler[i, 1].Equals(ComboBox1.Text))
                    {
                        gonderilen = int.Parse(kisiler[i, 0]);
                        break;
                    }
                }
            }

            string sql2 = "SELECT * FROM EngellenenKisiler WHERE engelleyenId = @engelleyen AND engellenenId = @engellenen";
            SqlParameter prms13 = new SqlParameter("@engelleyen", gonderilen);
            SqlParameter prms14 = new SqlParameter("@engellenen", Form1.kadi);
            SqlCommand cmd3 = new SqlCommand(sql2, conn);
            cmd3.Parameters.Add(prms13);
            cmd3.Parameters.Add(prms14);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd3);
            da2.Fill(dt4);
            if (dt4.Rows.Count > 0)
            {
                engel = true;
            }

            string sql1 = "SELECT TOP 1 Mesajlar.id AS SonKayit FROM Mesajlar ORDER BY id DESC";
            SqlCommand cmd2 = new SqlCommand(sql1, conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            da.Fill(dt3);
            int id = int.Parse(dt3.Rows[0][0].ToString());
            id++;
            string sql;
            if (gonderilen==0)
            {
                sql = "INSERT INTO Mesajlar(iletiSec, gonderenId, aliciId, baslik, mesaj, isOkundu, isSilindi, isGereksiz, isTaslak, olusturulmaTarihi) VALUES(@iletiSec, @gonderen, @alici, @baslik, @mesaj, @okundu, @silindi, @gereksiz, @taslak, @tarih);" +
                "INSERT INTO SikayetKutusu(mesajId, isCevaplandi) VALUES(@id ,@cevaplandi)";
            }
            else
            {
                sql = "INSERT INTO Mesajlar(iletiSec, gonderenId, aliciId, baslik, mesaj, isOkundu, isSilindi, isGereksiz, isTaslak, olusturulmaTarihi) VALUES(@iletiSec, @gonderen, @alici, @baslik, @mesaj, @okundu, @silindi, @gereksiz, @taslak, @tarih)";
            }
            
            SqlParameter prms1 = new SqlParameter("@iletiSec", false);
            SqlParameter prms2 = new SqlParameter("@gonderen", Form1.kadi);
            SqlParameter prms3 = new SqlParameter("@alici", gonderilen);
            SqlParameter prms4 = new SqlParameter("@baslik", txtbaslik.Text);
            SqlParameter prms5 = new SqlParameter("@mesaj", txtmesaj.Text);
            SqlParameter prms6 = new SqlParameter("@okundu", false);
            SqlParameter prms7 = new SqlParameter("@silindi", false);
            SqlParameter prms8;
            if (engel == false)
            {
                prms8 = new SqlParameter("@gereksiz", false);
            }
            else
            {
                prms8 = new SqlParameter("@gereksiz", true);
            }          
            SqlParameter prms9 = new SqlParameter("@taslak", false);
            SqlParameter prms10 = new SqlParameter("@tarih", DateTime.Now.Date);
            SqlParameter prms11 = new SqlParameter("@id", id);
            SqlParameter prms12 = new SqlParameter("@cevaplandi", false);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            cmd.Parameters.Add(prms2);
            cmd.Parameters.Add(prms3);
            cmd.Parameters.Add(prms4);
            cmd.Parameters.Add(prms5);
            cmd.Parameters.Add(prms6);
            cmd.Parameters.Add(prms7);
            cmd.Parameters.Add(prms8);
            cmd.Parameters.Add(prms9);
            cmd.Parameters.Add(prms10);
            cmd.Parameters.Add(prms11);
            cmd.Parameters.Add(prms12);
            cmd.ExecuteNonQuery();
            conn.Close();

            txtbaslik.Text = String.Empty;
            txtmesaj.Text = String.Empty;
            ComboBox1.Text = "Açılır Kutudan Kime Yollayacağınızı Seçin";
            MessageBox.Show("Mesaj başarıyla gönderildi!");
        }

        private void btnayarlar_Click(object sender, EventArgs e)
        {
            Ayarlar ayar = new Ayarlar();
            this.Visible = false;
            ayar.Show();
        }

        private void btnpanel_Click(object sender, EventArgs e)
        {
            Panel panel = new Panel();
            this.Visible = false;
            panel.Show();
        }
    }
}
