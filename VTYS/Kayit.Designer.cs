﻿namespace VTYS
{
    partial class Kayit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbKadi = new System.Windows.Forms.TextBox();
            this.tbAdi = new System.Windows.Forms.TextBox();
            this.tbMail = new System.Windows.Forms.TextBox();
            this.tbSifre = new System.Windows.Forms.TextBox();
            this.cbSoru = new System.Windows.Forms.ComboBox();
            this.tbCevap = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbText = new System.Windows.Forms.TextBox();
            this.tbKontrol = new System.Windows.Forms.TextBox();
            this.bKayit = new System.Windows.Forms.Button();
            this.tbSifreTekrar = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(83, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(233, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Yeni Hesap Almak İçin Aşağıdaki İşlemleri Yapın";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "* Kullanıcı Adı : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tam Adınız :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "* Mail Adresi :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "* Şifre :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Gizli Soru :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(44, 219);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "* Cevap :";
            // 
            // tbKadi
            // 
            this.tbKadi.Location = new System.Drawing.Point(162, 59);
            this.tbKadi.Name = "tbKadi";
            this.tbKadi.Size = new System.Drawing.Size(219, 20);
            this.tbKadi.TabIndex = 7;
            // 
            // tbAdi
            // 
            this.tbAdi.Location = new System.Drawing.Point(162, 85);
            this.tbAdi.Name = "tbAdi";
            this.tbAdi.Size = new System.Drawing.Size(219, 20);
            this.tbAdi.TabIndex = 8;
            // 
            // tbMail
            // 
            this.tbMail.Location = new System.Drawing.Point(162, 111);
            this.tbMail.Name = "tbMail";
            this.tbMail.Size = new System.Drawing.Size(219, 20);
            this.tbMail.TabIndex = 9;
            // 
            // tbSifre
            // 
            this.tbSifre.Location = new System.Drawing.Point(162, 137);
            this.tbSifre.Name = "tbSifre";
            this.tbSifre.Size = new System.Drawing.Size(219, 20);
            this.tbSifre.TabIndex = 10;
            // 
            // cbSoru
            // 
            this.cbSoru.FormattingEnabled = true;
            this.cbSoru.Location = new System.Drawing.Point(162, 189);
            this.cbSoru.Name = "cbSoru";
            this.cbSoru.Size = new System.Drawing.Size(219, 21);
            this.cbSoru.TabIndex = 11;
            this.cbSoru.Text = "Soru seçin :";
            // 
            // tbCevap
            // 
            this.tbCevap.Location = new System.Drawing.Point(162, 216);
            this.tbCevap.Name = "tbCevap";
            this.tbCevap.Size = new System.Drawing.Size(219, 20);
            this.tbCevap.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(138, 250);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(147, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Gördüğünüz Karakteri Giriniz :";
            // 
            // tbText
            // 
            this.tbText.Enabled = false;
            this.tbText.Location = new System.Drawing.Point(141, 279);
            this.tbText.Multiline = true;
            this.tbText.Name = "tbText";
            this.tbText.Size = new System.Drawing.Size(138, 51);
            this.tbText.TabIndex = 14;
            // 
            // tbKontrol
            // 
            this.tbKontrol.Location = new System.Drawing.Point(105, 336);
            this.tbKontrol.Name = "tbKontrol";
            this.tbKontrol.Size = new System.Drawing.Size(211, 20);
            this.tbKontrol.TabIndex = 15;
            // 
            // bKayit
            // 
            this.bKayit.Location = new System.Drawing.Point(105, 362);
            this.bKayit.Name = "bKayit";
            this.bKayit.Size = new System.Drawing.Size(211, 23);
            this.bKayit.TabIndex = 16;
            this.bKayit.Text = "Şimdi Kayıt Ol";
            this.bKayit.UseVisualStyleBackColor = true;
            this.bKayit.Click += new System.EventHandler(this.bKayit_Click);
            // 
            // tbSifreTekrar
            // 
            this.tbSifreTekrar.Location = new System.Drawing.Point(162, 163);
            this.tbSifreTekrar.Name = "tbSifreTekrar";
            this.tbSifreTekrar.Size = new System.Drawing.Size(219, 20);
            this.tbSifreTekrar.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(44, 166);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "* Şifreyi tekrar girin :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(44, 405);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(151, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "* : Doldurulması zorunlu alanlar";
            // 
            // Kayit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 434);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbSifreTekrar);
            this.Controls.Add(this.bKayit);
            this.Controls.Add(this.tbKontrol);
            this.Controls.Add(this.tbText);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbCevap);
            this.Controls.Add(this.cbSoru);
            this.Controls.Add(this.tbSifre);
            this.Controls.Add(this.tbMail);
            this.Controls.Add(this.tbAdi);
            this.Controls.Add(this.tbKadi);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Kayit";
            this.Text = "Kayıt Ekranı";
            this.Load += new System.EventHandler(this.Kayit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbKadi;
        private System.Windows.Forms.TextBox tbAdi;
        private System.Windows.Forms.TextBox tbMail;
        private System.Windows.Forms.TextBox tbSifre;
        private System.Windows.Forms.TextBox tbCevap;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbText;
        private System.Windows.Forms.TextBox tbKontrol;
        private System.Windows.Forms.Button bKayit;
        private System.Windows.Forms.TextBox tbSifreTekrar;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbSoru;
    }
}