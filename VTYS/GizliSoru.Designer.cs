﻿namespace VTYS
{
    partial class GizliSoru
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbSoru = new System.Windows.Forms.TextBox();
            this.tbCevap = new System.Windows.Forms.TextBox();
            this.tbSifre = new System.Windows.Forms.TextBox();
            this.tbSifreTekrar = new System.Windows.Forms.TextBox();
            this.bDegis = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Gizli Soru :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cevap :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Yeni Şifre :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Yeni Şifre Tekrar :";
            // 
            // tbSoru
            // 
            this.tbSoru.Enabled = false;
            this.tbSoru.Location = new System.Drawing.Point(131, 62);
            this.tbSoru.Name = "tbSoru";
            this.tbSoru.Size = new System.Drawing.Size(157, 20);
            this.tbSoru.TabIndex = 4;
            // 
            // tbCevap
            // 
            this.tbCevap.Location = new System.Drawing.Point(131, 88);
            this.tbCevap.Name = "tbCevap";
            this.tbCevap.Size = new System.Drawing.Size(157, 20);
            this.tbCevap.TabIndex = 5;
            // 
            // tbSifre
            // 
            this.tbSifre.Location = new System.Drawing.Point(131, 114);
            this.tbSifre.Name = "tbSifre";
            this.tbSifre.Size = new System.Drawing.Size(157, 20);
            this.tbSifre.TabIndex = 6;
            // 
            // tbSifreTekrar
            // 
            this.tbSifreTekrar.Location = new System.Drawing.Point(131, 140);
            this.tbSifreTekrar.Name = "tbSifreTekrar";
            this.tbSifreTekrar.Size = new System.Drawing.Size(157, 20);
            this.tbSifreTekrar.TabIndex = 7;
            // 
            // bDegis
            // 
            this.bDegis.Location = new System.Drawing.Point(131, 177);
            this.bDegis.Name = "bDegis";
            this.bDegis.Size = new System.Drawing.Size(157, 23);
            this.bDegis.TabIndex = 8;
            this.bDegis.Text = "Şifremi Değiştir";
            this.bDegis.UseVisualStyleBackColor = true;
            this.bDegis.Click += new System.EventHandler(this.bDegis_Click);
            // 
            // GizliSoru
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 261);
            this.Controls.Add(this.bDegis);
            this.Controls.Add(this.tbSifreTekrar);
            this.Controls.Add(this.tbSifre);
            this.Controls.Add(this.tbCevap);
            this.Controls.Add(this.tbSoru);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "GizliSoru";
            this.Text = "Şifre Değişimi";
            this.Load += new System.EventHandler(this.GizliSoru_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbSoru;
        private System.Windows.Forms.TextBox tbCevap;
        private System.Windows.Forms.TextBox tbSifre;
        private System.Windows.Forms.TextBox tbSifreTekrar;
        private System.Windows.Forms.Button bDegis;
    }
}