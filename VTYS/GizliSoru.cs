﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VTYS
{
    public partial class GizliSoru : Form
    {
        public GizliSoru()
        {
            InitializeComponent();
        }

        private void GizliSoru_Load(object sender, EventArgs e)
        {
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = "SELECT soru FROM Sorular WHERE id=@id";
            SqlParameter prms1 = new SqlParameter("@id", Form1.kullaniciAlanlari[0,0]);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            tbSoru.Text = dt.Rows[0][0].ToString();
            conn.Close();

        }

        private void bDegis_Click(object sender, EventArgs e)
        {
            if (!tbCevap.Text.ToString().Equals(Form1.kullaniciAlanlari[0,2]))
            {
                MessageBox.Show("Cevap yanlış girildi!");
            }
            else if (!tbSifre.Text.ToString().Equals(tbSifreTekrar.Text.ToString()))
            {
                MessageBox.Show("Şifreler aynı değil!");
            }
            else
            {
                string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
                SqlConnection conn = new SqlConnection(baglantiCumlesi);
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                string sql = "UPDATE Kisiler SET sifre=@sifre WHERE id=@id";
                SqlParameter prms1 = new SqlParameter("@sifre", tbSifre.Text);
                SqlParameter prms2 = new SqlParameter("@id", Form1.kullaniciAlanlari[0, 0]);
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.Add(prms1);
                cmd.Parameters.Add(prms2);
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Şifre başarılı bir şekilde değiştirildi!");
            }
        }
    }
}
