﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VTYS
{
    public partial class Ayarlar : Form
    {
        public Ayarlar()
        {
            InitializeComponent();
        }

        DataTable dt = new DataTable();
        public void listele() {
            dt.Clear();
            ListBox1.Items.Clear();
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = "SELECT Kisiler.kullaniciAdi FROM Kisiler INNER JOIN EngellenenKisiler ON Kisiler.id = EngellenenKisiler.engellenenId WHERE EngellenenKisiler.engelleyenId = @engelleyen";
            SqlParameter prms1 = new SqlParameter("@engelleyen", Form1.kadi);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            SqlDataReader dr = cmd.ExecuteReader();         
            
            int i = 0;
            while (dr.Read())
            {
                ListBox1.Items.Add(dt.Rows[i][0].ToString());
                i++;
            }
            conn.Close();
            
        }

        private void btndegistir_Click(object sender, EventArgs e)
        {
            if (txtpass.Text=="" && txtpass2.Text=="")
            {
                MessageBox.Show("Lütfen bir şifre girin!");
            }
            else if (txtpass.Text == txtpass2.Text)
            {
                string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
                SqlConnection conn = new SqlConnection(baglantiCumlesi);
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                string sql = "UPDATE Kisiler SET sifre = @sifre where id=@id";
                SqlParameter prms1 = new SqlParameter("@sifre", txtpass.Text);
                SqlParameter prms2 = new SqlParameter("@id", Form1.kadi);
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.Add(prms1);
                cmd.Parameters.Add(prms2);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Şifre başarılı bir şekilde değiştirildi");
                conn.Close();
            }
            else if (txtpass.Text != txtpass2.Text)
            {
                MessageBox.Show("Şifreler aynı değil!");
            }
        }

        private void btnengelle_Click(object sender, EventArgs e)
        {
            dt.Clear(); 
            string isim = txtkisi.Text;
            int engellenenId=0;

            if (isim=="")
            {
                MessageBox.Show("Lütfen bir kullanıcı girin!");
                return;
            }

            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql1 = "SELECT id FROM Kisiler WHERE kullaniciAdi=@kadi";
            SqlParameter prms1 = new SqlParameter("@kadi", isim);
            SqlCommand cmd = new SqlCommand(sql1, conn);
            cmd.Parameters.Add(prms1);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("Böyle bir kullanıcı bulunmamaktadır!");
                conn.Close();
                return;
            }
            else
            {
                engellenenId = int.Parse(dt.Rows[0][1].ToString());
            }

            DataTable dt2 = new DataTable();
            string sql2 = "SELECT * FROM EngellenenKisiler WHERE engelleyenId = @engelleyen AND engellenenId = @engellenen";
            SqlParameter prms2 = new SqlParameter("@engelleyen", Form1.kadi);
            SqlParameter prms3 = new SqlParameter("@engellenen", engellenenId);
            SqlCommand cmd2 = new SqlCommand(sql2, conn);
            dt.Clear();
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            cmd2.Parameters.Add(prms2);
            cmd2.Parameters.Add(prms3);
            da2.Fill(dt);
            if (dt.Rows.Count == 1)
            {
                MessageBox.Show("Bu kullanıcı zaten engellenmiş!");
                conn.Close();
                return;
            }
            string sql = "INSERT INTO EngellenenKisiler(engelleyenId, engellenenId) VALUES(@engelleyenId, @engellenenId)";
            SqlParameter prms4 = new SqlParameter("@engelleyenId", Form1.kadi);
            SqlParameter prms5 = new SqlParameter("@engellenenId", engellenenId);
            SqlCommand cmd3 = new SqlCommand(sql, conn);
            cmd3.Parameters.Add(prms4);
            cmd3.Parameters.Add(prms5);
            cmd3.ExecuteNonQuery();
            MessageBox.Show("Kişi başarılı bir şekilde engellendi!");
            conn.Close();
            listele();
        }

        private void Ayarlar_Load(object sender, EventArgs e)
        {
            listele();
        }

        private void btnkaldir_Click(object sender, EventArgs e)
        {
            if (ListBox1.SelectedItem.ToString()=="")
            {
                MessageBox.Show("Lütfen bir kullanıcı seçin!");
                return;
            }
            dt.Clear();
            string secilen = ListBox1.SelectedItem.ToString();
            int id;
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql1 = "SELECT id FROM Kisiler WHERE kullaniciAdi=@kadi";
            SqlParameter prms1 = new SqlParameter("@kadi", secilen);
            SqlCommand cmd = new SqlCommand(sql1, conn);
            cmd.Parameters.Add(prms1);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            id = int.Parse(dt.Rows[0][1].ToString());
            string sql2 = "DELETE FROM EngellenenKisiler WHERE engellenenId = @engellenen";
            SqlParameter prms2 = new SqlParameter("@engellenen", id);
            SqlCommand cmd2 = new SqlCommand(sql2, conn);
            cmd2.Parameters.Add(prms2);
            cmd2.ExecuteNonQuery();
            conn.Close();
            MessageBox.Show("Kullanıcı başarılı bir şekilde engellenen kişilerden kaldırıldı.");
            listele();
        }
    }
}
