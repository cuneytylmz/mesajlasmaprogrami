﻿namespace VTYS
{
    partial class Mailler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnokundu = new System.Windows.Forms.Button();
            this.btnpanel = new System.Windows.Forms.Button();
            this.btngonder = new System.Windows.Forms.Button();
            this.btnkaydet = new System.Windows.Forms.Button();
            this.ComboBox1 = new System.Windows.Forms.ComboBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtmesaj = new System.Windows.Forms.TextBox();
            this.txtbaslik = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.btnsil = new System.Windows.Forms.Button();
            this.btnyeni = new System.Windows.Forms.Button();
            this.btnayarlar = new System.Windows.Forms.Button();
            this.btngereksiz = new System.Windows.Forms.Button();
            this.btnsilinmis = new System.Windows.Forms.Button();
            this.btntaslaklar = new System.Windows.Forms.Button();
            this.btngonderilen = new System.Windows.Forms.Button();
            this.btngelen = new System.Windows.Forms.Button();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnokundu
            // 
            this.btnokundu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnokundu.Location = new System.Drawing.Point(534, 419);
            this.btnokundu.Name = "btnokundu";
            this.btnokundu.Size = new System.Drawing.Size(180, 23);
            this.btnokundu.TabIndex = 46;
            this.btnokundu.Text = " Okundu Olarak İşaretle";
            this.btnokundu.UseVisualStyleBackColor = true;
            this.btnokundu.Click += new System.EventHandler(this.btnokundu_Click);
            // 
            // btnpanel
            // 
            this.btnpanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnpanel.Location = new System.Drawing.Point(39, 204);
            this.btnpanel.Name = "btnpanel";
            this.btnpanel.Size = new System.Drawing.Size(116, 23);
            this.btnpanel.TabIndex = 45;
            this.btnpanel.Text = "Panel";
            this.btnpanel.UseVisualStyleBackColor = true;
            this.btnpanel.Visible = false;
            this.btnpanel.Click += new System.EventHandler(this.btnpanel_Click);
            // 
            // btngonder
            // 
            this.btngonder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btngonder.Location = new System.Drawing.Point(438, 296);
            this.btngonder.Name = "btngonder";
            this.btngonder.Size = new System.Drawing.Size(92, 47);
            this.btngonder.TabIndex = 44;
            this.btngonder.Text = "Gönder";
            this.btngonder.UseVisualStyleBackColor = true;
            this.btngonder.Visible = false;
            this.btngonder.Click += new System.EventHandler(this.btngonder_Click);
            // 
            // btnkaydet
            // 
            this.btnkaydet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnkaydet.Location = new System.Drawing.Point(536, 296);
            this.btnkaydet.Name = "btnkaydet";
            this.btnkaydet.Size = new System.Drawing.Size(92, 47);
            this.btnkaydet.TabIndex = 43;
            this.btnkaydet.Text = "Taslaklara Kaydet";
            this.btnkaydet.UseVisualStyleBackColor = true;
            this.btnkaydet.Visible = false;
            this.btnkaydet.Click += new System.EventHandler(this.btnkaydet_Click);
            // 
            // ComboBox1
            // 
            this.ComboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ComboBox1.FormattingEnabled = true;
            this.ComboBox1.Location = new System.Drawing.Point(365, 35);
            this.ComboBox1.Name = "ComboBox1";
            this.ComboBox1.Size = new System.Drawing.Size(327, 24);
            this.ComboBox1.TabIndex = 42;
            this.ComboBox1.Text = "Açılır Kutudan Kime Yollayacağınızı Seçin";
            this.ComboBox1.Visible = false;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.BackColor = System.Drawing.Color.Transparent;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Label3.Location = new System.Drawing.Point(306, 38);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(50, 16);
            this.Label3.TabIndex = 41;
            this.Label3.Text = "Kime :";
            this.Label3.Visible = false;
            // 
            // txtmesaj
            // 
            this.txtmesaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtmesaj.Location = new System.Drawing.Point(365, 88);
            this.txtmesaj.Multiline = true;
            this.txtmesaj.Name = "txtmesaj";
            this.txtmesaj.Size = new System.Drawing.Size(327, 202);
            this.txtmesaj.TabIndex = 40;
            this.txtmesaj.Visible = false;
            // 
            // txtbaslik
            // 
            this.txtbaslik.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtbaslik.Location = new System.Drawing.Point(365, 64);
            this.txtbaslik.Name = "txtbaslik";
            this.txtbaslik.Size = new System.Drawing.Size(327, 22);
            this.txtbaslik.TabIndex = 39;
            this.txtbaslik.Visible = false;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Label2.Location = new System.Drawing.Point(306, 91);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(58, 16);
            this.Label2.TabIndex = 38;
            this.Label2.Text = "Mesaj :";
            this.Label2.Visible = false;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Label1.Location = new System.Drawing.Point(306, 65);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(59, 16);
            this.Label1.TabIndex = 37;
            this.Label1.Text = "Başlık :";
            this.Label1.Visible = false;
            // 
            // btnsil
            // 
            this.btnsil.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnsil.Location = new System.Drawing.Point(437, 419);
            this.btnsil.Name = "btnsil";
            this.btnsil.Size = new System.Drawing.Size(91, 23);
            this.btnsil.TabIndex = 36;
            this.btnsil.Text = "Sil";
            this.btnsil.UseVisualStyleBackColor = true;
            this.btnsil.Click += new System.EventHandler(this.btnsil_Click);
            // 
            // btnyeni
            // 
            this.btnyeni.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnyeni.Location = new System.Drawing.Point(340, 419);
            this.btnyeni.Name = "btnyeni";
            this.btnyeni.Size = new System.Drawing.Size(91, 23);
            this.btnyeni.TabIndex = 35;
            this.btnyeni.Text = "Yeni Mesaj";
            this.btnyeni.UseVisualStyleBackColor = true;
            this.btnyeni.Click += new System.EventHandler(this.btnyeni_Click);
            // 
            // btnayarlar
            // 
            this.btnayarlar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnayarlar.Location = new System.Drawing.Point(39, 175);
            this.btnayarlar.Name = "btnayarlar";
            this.btnayarlar.Size = new System.Drawing.Size(116, 23);
            this.btnayarlar.TabIndex = 34;
            this.btnayarlar.Text = "Ayarlar";
            this.btnayarlar.UseVisualStyleBackColor = true;
            this.btnayarlar.Click += new System.EventHandler(this.btnayarlar_Click);
            // 
            // btngereksiz
            // 
            this.btngereksiz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btngereksiz.Location = new System.Drawing.Point(39, 59);
            this.btngereksiz.Name = "btngereksiz";
            this.btngereksiz.Size = new System.Drawing.Size(116, 23);
            this.btngereksiz.TabIndex = 33;
            this.btngereksiz.Text = "Gereksiz";
            this.btngereksiz.UseVisualStyleBackColor = true;
            this.btngereksiz.Click += new System.EventHandler(this.btngereksiz_Click);
            // 
            // btnsilinmis
            // 
            this.btnsilinmis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnsilinmis.Location = new System.Drawing.Point(39, 146);
            this.btnsilinmis.Name = "btnsilinmis";
            this.btnsilinmis.Size = new System.Drawing.Size(116, 23);
            this.btnsilinmis.TabIndex = 32;
            this.btnsilinmis.Text = "Silinmiş";
            this.btnsilinmis.UseVisualStyleBackColor = true;
            this.btnsilinmis.Click += new System.EventHandler(this.btnsilinmis_Click);
            // 
            // btntaslaklar
            // 
            this.btntaslaklar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btntaslaklar.Location = new System.Drawing.Point(39, 88);
            this.btntaslaklar.Name = "btntaslaklar";
            this.btntaslaklar.Size = new System.Drawing.Size(116, 23);
            this.btntaslaklar.TabIndex = 31;
            this.btntaslaklar.Text = "Taslaklar";
            this.btntaslaklar.UseVisualStyleBackColor = true;
            this.btntaslaklar.Click += new System.EventHandler(this.btntaslaklar_Click);
            // 
            // btngonderilen
            // 
            this.btngonderilen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btngonderilen.Location = new System.Drawing.Point(39, 117);
            this.btngonderilen.Name = "btngonderilen";
            this.btngonderilen.Size = new System.Drawing.Size(116, 23);
            this.btngonderilen.TabIndex = 30;
            this.btngonderilen.Text = "Gönderilenler";
            this.btngonderilen.UseVisualStyleBackColor = true;
            this.btngonderilen.Click += new System.EventHandler(this.btngonderilen_Click);
            // 
            // btngelen
            // 
            this.btngelen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btngelen.Location = new System.Drawing.Point(39, 30);
            this.btngelen.Name = "btngelen";
            this.btngelen.Size = new System.Drawing.Size(116, 23);
            this.btngelen.TabIndex = 29;
            this.btngelen.Text = "Gelen Kutusu";
            this.btngelen.UseVisualStyleBackColor = true;
            this.btngelen.Click += new System.EventHandler(this.btngelen_Click);
            // 
            // DataGridView1
            // 
            this.DataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(173, 30);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.Size = new System.Drawing.Size(646, 383);
            this.DataGridView1.TabIndex = 28;
            // 
            // Mailler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 489);
            this.Controls.Add(this.btnokundu);
            this.Controls.Add(this.btnpanel);
            this.Controls.Add(this.btngonder);
            this.Controls.Add(this.btnkaydet);
            this.Controls.Add(this.ComboBox1);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.txtmesaj);
            this.Controls.Add(this.txtbaslik);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.btnsil);
            this.Controls.Add(this.btnyeni);
            this.Controls.Add(this.btnayarlar);
            this.Controls.Add(this.btngereksiz);
            this.Controls.Add(this.btnsilinmis);
            this.Controls.Add(this.btntaslaklar);
            this.Controls.Add(this.btngonderilen);
            this.Controls.Add(this.btngelen);
            this.Controls.Add(this.DataGridView1);
            this.Name = "Mailler";
            this.Text = "Mailler";
            this.Load += new System.EventHandler(this.Mailler_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnokundu;
        internal System.Windows.Forms.Button btnpanel;
        internal System.Windows.Forms.Button btngonder;
        internal System.Windows.Forms.Button btnkaydet;
        internal System.Windows.Forms.ComboBox ComboBox1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox txtmesaj;
        internal System.Windows.Forms.TextBox txtbaslik;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button btnsil;
        internal System.Windows.Forms.Button btnyeni;
        internal System.Windows.Forms.Button btnayarlar;
        internal System.Windows.Forms.Button btngereksiz;
        internal System.Windows.Forms.Button btnsilinmis;
        internal System.Windows.Forms.Button btntaslaklar;
        internal System.Windows.Forms.Button btngonderilen;
        internal System.Windows.Forms.Button btngelen;
        internal System.Windows.Forms.DataGridView DataGridView1;
    }
}