﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VTYS
{
    public partial class Panel : Form
    {
        public Panel()
        {
            InitializeComponent();
        }

        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        DataTable dt3 = new DataTable();
        string[,] kisiler = new string[100, 2];

        public void gelen()
        {
            dt.Clear();
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            string sql = "SELECT Mesajlar.id as ID, Mesajlar.iletiSec as İletiSeç, Kisiler.kullaniciAdi as Gönderen, Mesajlar.baslik as Başlık, Mesajlar.mesaj as Mesaj, Mesajlar.isOkundu as Okundu, Mesajlar.olusturulmaTarihi as Tarih, SikayetKutusu.isCevaplandi as Cevaplandi FROM Mesajlar INNER JOIN SikayetKutusu ON Mesajlar.id = SikayetKutusu.mesajId INNER JOIN Kisiler ON Mesajlar.gonderenId = Kisiler.id WHERE Mesajlar.aliciId = 0 AND Mesajlar.isSilindi = @silindi AND Mesajlar.isTaslak = @taslak";
            SqlParameter prms1 = new SqlParameter("@silindi", false);
            SqlParameter prms2 = new SqlParameter("@taslak", false);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            cmd.Parameters.Add(prms2);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            DataGridView1.DataSource = dt;
            conn.Close();
        }

        public void IDkontrol()
        {
            if (DataGridView1.Columns[0].Name.ToString().Equals("ID"))
            {
                DataGridView1.Columns.Remove("ID");

            }
        }

        public void goster()
        {
            Label1.Visible = false;
            Label2.Visible = false;
            Label3.Visible = false;
            ComboBox1.Visible = false;
            txtbaslik.Visible = false;
            txtmesaj.Visible = false;
            btngonder.Visible = false;
            btnkaydet.Visible = false;
            DataGridView1.Visible = true;
            btnsil.Visible = true;
            btnokundu.Visible = true;
        }

        public void gizle()
        {
            Label1.Visible = true;
            Label2.Visible = true;
            Label3.Visible = true;
            ComboBox1.Visible = true;
            txtbaslik.Visible = true;
            txtmesaj.Visible = true;
            btngonder.Visible = true;
            btnkaydet.Visible = true;
            DataGridView1.Visible = false;
            btnsil.Visible = false;
            btnokundu.Visible = false;
        }

        private void Panel_Load(object sender, EventArgs e)
        {
            gelen();
            IDkontrol();
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            goster();
            gelen();
            IDkontrol();
        }

        private void btnYolla_Click(object sender, EventArgs e)
        {
            txtbaslik.Text = String.Empty;
            txtmesaj.Text = String.Empty;
            ComboBox1.Items.Clear();
            gizle();
            dt2.Clear();
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = "SELECT id, kullaniciAdi, kisilerType FROM Kisiler WHERE NOT id=0";
            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt2);
            SqlDataReader dr = cmd.ExecuteReader();
            int i = 0;
            int j = 0;
            while (dr.Read())
            {
                if (dt2.Rows[i][2].ToString().Equals("K"))
                {
                    kisiler[j, 0] = dt2.Rows[i][0].ToString();
                    kisiler[j, 1] = dt2.Rows[i][1].ToString();
                    j++;
                    ComboBox1.Items.Add(dt2.Rows[i][1].ToString());
                }
                i = i + 1;
            }
            conn.Close();
        }

        private void btngonder_Click(object sender, EventArgs e)
        {
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            int? gonderilen = null;
            if (ComboBox1.Text == "Açılır Kutudan Kime Yollayacağınızı Seçin")
            {
                MessageBox.Show("Lütfen açılır kutudan kime yollayacağınızı seçin!");
                return;
            }
            else
            {
                for (int i = 0; i < kisiler.Length; i++)
                {
                    if (kisiler[i, 1].Equals(ComboBox1.Text))
                    {
                        gonderilen = int.Parse(kisiler[i, 0]);
                        break;
                    }
                }
            }
            string sql1 = "SELECT TOP 1 Mesajlar.id AS SonKayit FROM Mesajlar ORDER BY id DESC";
            SqlCommand cmd2 = new SqlCommand(sql1, conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            da.Fill(dt3);
            int id = int.Parse(dt3.Rows[0][0].ToString());
            id++;

            string sql;
            sql = "INSERT INTO Mesajlar(iletiSec, gonderenId, aliciId, baslik, mesaj, isOkundu, isSilindi, isGereksiz, isTaslak, olusturulmaTarihi) VALUES(@iletiSec, 0, @alici, @baslik, @mesaj, @okundu, @silindi, @gereksiz, @taslak, @tarih)";
            SqlParameter prms1 = new SqlParameter("@iletiSec", false);
            SqlParameter prms3 = new SqlParameter("@alici", gonderilen);
            SqlParameter prms4 = new SqlParameter("@baslik", txtbaslik.Text);
            SqlParameter prms5 = new SqlParameter("@mesaj", txtmesaj.Text);
            SqlParameter prms6 = new SqlParameter("@okundu", false);
            SqlParameter prms7 = new SqlParameter("@silindi", false);
            SqlParameter prms8 = new SqlParameter("@gereksiz", false);
            SqlParameter prms9 = new SqlParameter("@taslak", false);
            SqlParameter prms10 = new SqlParameter("@tarih", DateTime.Now.Date);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            cmd.Parameters.Add(prms3);
            cmd.Parameters.Add(prms4);
            cmd.Parameters.Add(prms5);
            cmd.Parameters.Add(prms6);
            cmd.Parameters.Add(prms7);
            cmd.Parameters.Add(prms8);
            cmd.Parameters.Add(prms9);
            cmd.Parameters.Add(prms10);
            cmd.ExecuteNonQuery();
            conn.Close();

            txtbaslik.Text = String.Empty;
            txtmesaj.Text = String.Empty;
            ComboBox1.Text = "Açılır Kutudan Kime Yollayacağınızı Seçin";
            MessageBox.Show("Mesaj başarıyla gönderildi!");
        }

        private void btnkaydet_Click(object sender, EventArgs e)
        {
            int? gonderilen = null;
            if (ComboBox1.Text == "Açılır Kutudan Kime Yollayacağınızı Seçin")
            {
                MessageBox.Show("Lütfen açılır kutudan kime yollayacağınızı seçin!");
                return;
            }
            else
            {
                for (int i = 0; i < kisiler.Length; i++)
                {
                    if (kisiler[i, 1].Equals(ComboBox1.Text))
                    {
                        gonderilen = int.Parse(kisiler[i, 0]);
                        break;
                    }

                }
            }
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = "INSERT INTO Mesajlar(iletiSec, gonderenId, aliciId, baslik, mesaj, isOkundu, isSilindi, isGereksiz, isTaslak, olusturulmaTarihi) VALUES(@iletiSec, 0, @alici, @baslik, @mesaj, @okundu, @silindi, @gereksiz, @taslak, @tarih)";
            SqlParameter prms1 = new SqlParameter("@iletiSec", false);
            SqlParameter prms3 = new SqlParameter("@alici", gonderilen);
            SqlParameter prms4 = new SqlParameter("@baslik", txtbaslik.Text);
            SqlParameter prms5 = new SqlParameter("@mesaj", txtmesaj.Text);
            SqlParameter prms6 = new SqlParameter("@okundu", false);
            SqlParameter prms7 = new SqlParameter("@silindi", false);
            SqlParameter prms8 = new SqlParameter("@gereksiz", false);
            SqlParameter prms9 = new SqlParameter("@taslak", true);
            SqlParameter prms10 = new SqlParameter("@tarih", DateTime.Now.Date);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add(prms1);
            cmd.Parameters.Add(prms3);
            cmd.Parameters.Add(prms4);
            cmd.Parameters.Add(prms5);
            cmd.Parameters.Add(prms6);
            cmd.Parameters.Add(prms7);
            cmd.Parameters.Add(prms8);
            cmd.Parameters.Add(prms9);
            cmd.Parameters.Add(prms10);
            cmd.ExecuteNonQuery();
            txtbaslik.Text = String.Empty;
            txtmesaj.Text = String.Empty;
            ComboBox1.Text = "Açılır Kutudan Kime Yollayacağınızı Seçin";
            MessageBox.Show("Mesaj taslaklara kaydedildi!");
        }

        private void btnsil_Click(object sender, EventArgs e)
        {
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql;
            SqlParameter prms1;
            SqlCommand cmd;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["İletiSeç"].Equals(true))
                {
                    sql = "DELETE FROM SikayetKutusu WHERE mesajId = @id";
                    prms1 = new SqlParameter("@id", dt.Rows[i][0]);
                    cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.Add(prms1);
                    cmd.ExecuteNonQuery();
                }
            }
            conn.Close();
            MessageBox.Show("Kayıt(lar) Silindi!");
        }
        
        private void btnokundu_Click(object sender, EventArgs e)
        {
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql;
            SqlParameter prms1, prms2;
            SqlCommand cmd;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["Okundu"].Equals(true))
                {
                    sql = "UPDATE Mesajlar SET isOkundu=@okundu WHERE id=@id";
                    prms1 = new SqlParameter("@okundu", true);
                    prms2 = new SqlParameter("@id", dt.Rows[i][0]);
                    cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.Add(prms1);
                    cmd.Parameters.Add(prms2);        
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    sql = "UPDATE Mesajlar SET isOkundu=@okundu WHERE id=@id";
                    prms1 = new SqlParameter("@okundu", false);
                    prms2 = new SqlParameter("@id", dt.Rows[i][0]);
                    cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.Add(prms1);
                    cmd.Parameters.Add(prms2);
                    cmd.ExecuteNonQuery();
                }
            }
            MessageBox.Show("Kayıt(lar) Güncellendi!");
            conn.Close();
        }

        private void btnCevap_Click(object sender, EventArgs e)
        {
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql;
            SqlParameter prms1, prms2;
            SqlCommand cmd;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["Cevaplandi"].Equals(true))
                {
                    sql = "UPDATE SikayetKutusu SET isCevaplandi=@cevaplandi WHERE mesajId=@id";
                    prms1 = new SqlParameter("@cevaplandi", true);
                    prms2 = new SqlParameter("@id", dt.Rows[i][0]);
                    cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.Add(prms1);
                    cmd.Parameters.Add(prms2);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    sql = "UPDATE SikayetKutusu SET isCevaplandi=@cevaplandi WHERE mesajId=@id";
                    prms1 = new SqlParameter("@cevaplandi", false);
                    prms2 = new SqlParameter("@id", dt.Rows[i][0]);
                    cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.Add(prms1);
                    cmd.Parameters.Add(prms2);
                    cmd.ExecuteNonQuery();
                }
            }
            MessageBox.Show("Kayıt(lar) Güncellendi!");
            conn.Close();
        }
    }
}
