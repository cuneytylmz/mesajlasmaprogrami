﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VTYS
{
    public partial class Kayit : Form
    {
        public Kayit()
        {
            InitializeComponent();
        }

        string[,] sorular = new string[10, 2];

        public void rastgele()
        {
            tbText.Text = "";
            string harfler;
            int rakam, rastgeleGelen;
            string[] harf = new string[29];
            harfler = "a b c ç d e f g ğ h ı i j k l m n o ö p r s ş t u ü v y z";
            harf = harfler.Split(' ');
            Random rastgele = new Random();
            for (int i = 0; i < 3; i++)
            {
                rastgeleGelen = rastgele.Next(29);
                tbText.Text = tbText.Text.ToString() + harf[rastgeleGelen].ToString();
                rakam = rastgele.Next(10);
                tbText.Text = tbText.Text.ToString() + rakam.ToString();
            }
        }

        private void Kayit_Load(object sender, EventArgs e)
        {
            rastgele();
            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = "SELECT * FROM Sorular";
            SqlCommand cmd = new SqlCommand(sql, conn);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            int i = -1;
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i = i + 1;
                sorular[i, 1] = dt.Rows[i][1].ToString();
                cbSoru.Items.Add(dt.Rows[i][1].ToString());
            }
            conn.Close();
        }

        private void bKayit_Click(object sender, EventArgs e)
        {
            int secilenSoru = 0;
            for (int i = 0; i < sorular.Length; i++)
            {
                if (cbSoru.Text.ToString().Equals(sorular[i, 1]))
                {
                    secilenSoru = i;
                    break;
                }
            }
            string baglantiCumlesi2 = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn2 = new SqlConnection(baglantiCumlesi2);
            if (conn2.State == ConnectionState.Closed)
            {
                conn2.Open();
            }
            string sql2 = "SELECT TOP 1 id FROM Kisiler ORDER BY id DESC";
            SqlCommand cmd2 = new SqlCommand(sql2, conn2);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            da.Fill(dt);
            conn2.Close();
            int id = int.Parse(dt.Rows[0][0].ToString());
            id = id + 1;

            string baglantiCumlesi = @"Data Source=.\MSSQLSERVER2014;Initial Catalog=Mesajlasma;Integrated Security=true;";
            SqlConnection conn = new SqlConnection(baglantiCumlesi);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            SqlParameter prms1 = new SqlParameter("@kadi", tbKadi.Text);
            SqlParameter prms2 = new SqlParameter("@tamAdi", tbAdi.Text);
            SqlParameter prms3 = new SqlParameter("@mail", tbMail.Text);
            SqlParameter prms4 = new SqlParameter("@sifre", tbSifre.Text);
            SqlParameter prms5 = new SqlParameter("@soru", secilenSoru);
            SqlParameter prms6 = new SqlParameter("@cevap", tbCevap.Text);
            SqlParameter prms7 = new SqlParameter("@tip", 'K');
            SqlParameter prms8 = new SqlParameter("@kullaniciId", id);
            SqlCommand cmd = new SqlCommand("KisiEkle", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(prms1);
            cmd.Parameters.Add(prms2);
            cmd.Parameters.Add(prms3);
            cmd.Parameters.Add(prms4);
            cmd.Parameters.Add(prms5);
            cmd.Parameters.Add(prms6);
            cmd.Parameters.Add(prms7);
            cmd.Parameters.Add(prms8);
            cmd.ExecuteNonQuery();
            conn.Close();
            MessageBox.Show("Kayıt başarılı bir şekilde eklendi!");

        }
        }
    }

