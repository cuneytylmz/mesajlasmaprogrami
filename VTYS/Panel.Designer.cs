﻿namespace VTYS
{
    partial class Panel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnokundu = new System.Windows.Forms.Button();
            this.btngonder = new System.Windows.Forms.Button();
            this.btnkaydet = new System.Windows.Forms.Button();
            this.ComboBox1 = new System.Windows.Forms.ComboBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtmesaj = new System.Windows.Forms.TextBox();
            this.txtbaslik = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.btnMail = new System.Windows.Forms.Button();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnYolla = new System.Windows.Forms.Button();
            this.btnsil = new System.Windows.Forms.Button();
            this.btnCevap = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnokundu
            // 
            this.btnokundu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnokundu.Location = new System.Drawing.Point(456, 429);
            this.btnokundu.Name = "btnokundu";
            this.btnokundu.Size = new System.Drawing.Size(191, 36);
            this.btnokundu.TabIndex = 65;
            this.btnokundu.Text = " Okundu Olarak İşaretle";
            this.btnokundu.UseVisualStyleBackColor = true;
            this.btnokundu.Click += new System.EventHandler(this.btnokundu_Click);
            // 
            // btngonder
            // 
            this.btngonder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btngonder.Location = new System.Drawing.Point(457, 306);
            this.btngonder.Name = "btngonder";
            this.btngonder.Size = new System.Drawing.Size(92, 47);
            this.btngonder.TabIndex = 63;
            this.btngonder.Text = "Gönder";
            this.btngonder.UseVisualStyleBackColor = true;
            this.btngonder.Visible = false;
            this.btngonder.Click += new System.EventHandler(this.btngonder_Click);
            // 
            // btnkaydet
            // 
            this.btnkaydet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnkaydet.Location = new System.Drawing.Point(555, 306);
            this.btnkaydet.Name = "btnkaydet";
            this.btnkaydet.Size = new System.Drawing.Size(92, 47);
            this.btnkaydet.TabIndex = 62;
            this.btnkaydet.Text = "Taslaklara Kaydet";
            this.btnkaydet.UseVisualStyleBackColor = true;
            this.btnkaydet.Visible = false;
            this.btnkaydet.Click += new System.EventHandler(this.btnkaydet_Click);
            // 
            // ComboBox1
            // 
            this.ComboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ComboBox1.FormattingEnabled = true;
            this.ComboBox1.Location = new System.Drawing.Point(384, 45);
            this.ComboBox1.Name = "ComboBox1";
            this.ComboBox1.Size = new System.Drawing.Size(327, 24);
            this.ComboBox1.TabIndex = 61;
            this.ComboBox1.Text = "Açılır Kutudan Kime Yollayacağınızı Seçin";
            this.ComboBox1.Visible = false;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.BackColor = System.Drawing.Color.Transparent;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Label3.Location = new System.Drawing.Point(325, 48);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(50, 16);
            this.Label3.TabIndex = 60;
            this.Label3.Text = "Kime :";
            this.Label3.Visible = false;
            // 
            // txtmesaj
            // 
            this.txtmesaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtmesaj.Location = new System.Drawing.Point(384, 98);
            this.txtmesaj.Multiline = true;
            this.txtmesaj.Name = "txtmesaj";
            this.txtmesaj.Size = new System.Drawing.Size(327, 202);
            this.txtmesaj.TabIndex = 59;
            this.txtmesaj.Visible = false;
            // 
            // txtbaslik
            // 
            this.txtbaslik.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtbaslik.Location = new System.Drawing.Point(384, 74);
            this.txtbaslik.Name = "txtbaslik";
            this.txtbaslik.Size = new System.Drawing.Size(327, 22);
            this.txtbaslik.TabIndex = 58;
            this.txtbaslik.Visible = false;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Label2.Location = new System.Drawing.Point(325, 101);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(58, 16);
            this.Label2.TabIndex = 57;
            this.Label2.Text = "Mesaj :";
            this.Label2.Visible = false;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Label1.Location = new System.Drawing.Point(325, 75);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(59, 16);
            this.Label1.TabIndex = 56;
            this.Label1.Text = "Başlık :";
            this.Label1.Visible = false;
            // 
            // btnMail
            // 
            this.btnMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnMail.Location = new System.Drawing.Point(19, 37);
            this.btnMail.Name = "btnMail";
            this.btnMail.Size = new System.Drawing.Size(116, 51);
            this.btnMail.TabIndex = 48;
            this.btnMail.Text = "Mailleri Göster";
            this.btnMail.UseVisualStyleBackColor = true;
            this.btnMail.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // DataGridView1
            // 
            this.DataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(153, 37);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.Size = new System.Drawing.Size(747, 383);
            this.DataGridView1.TabIndex = 47;
            // 
            // btnYolla
            // 
            this.btnYolla.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnYolla.Location = new System.Drawing.Point(19, 98);
            this.btnYolla.Name = "btnYolla";
            this.btnYolla.Size = new System.Drawing.Size(116, 51);
            this.btnYolla.TabIndex = 66;
            this.btnYolla.Text = "Mail Yolla";
            this.btnYolla.UseVisualStyleBackColor = true;
            this.btnYolla.Click += new System.EventHandler(this.btnYolla_Click);
            // 
            // btnsil
            // 
            this.btnsil.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnsil.Location = new System.Drawing.Point(264, 429);
            this.btnsil.Name = "btnsil";
            this.btnsil.Size = new System.Drawing.Size(186, 36);
            this.btnsil.TabIndex = 55;
            this.btnsil.Text = "Sil";
            this.btnsil.UseVisualStyleBackColor = true;
            this.btnsil.Click += new System.EventHandler(this.btnsil_Click);
            // 
            // btnCevap
            // 
            this.btnCevap.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnCevap.Location = new System.Drawing.Point(653, 429);
            this.btnCevap.Name = "btnCevap";
            this.btnCevap.Size = new System.Drawing.Size(207, 36);
            this.btnCevap.TabIndex = 67;
            this.btnCevap.Text = "Cevaplandı Olarak İşaretle";
            this.btnCevap.UseVisualStyleBackColor = true;
            this.btnCevap.Click += new System.EventHandler(this.btnCevap_Click);
            // 
            // Panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 487);
            this.Controls.Add(this.btnCevap);
            this.Controls.Add(this.btnYolla);
            this.Controls.Add(this.btnokundu);
            this.Controls.Add(this.btngonder);
            this.Controls.Add(this.btnkaydet);
            this.Controls.Add(this.ComboBox1);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.txtmesaj);
            this.Controls.Add(this.txtbaslik);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.btnsil);
            this.Controls.Add(this.btnMail);
            this.Controls.Add(this.DataGridView1);
            this.Name = "Panel";
            this.Text = "Panel";
            this.Load += new System.EventHandler(this.Panel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnokundu;
        internal System.Windows.Forms.Button btngonder;
        internal System.Windows.Forms.Button btnkaydet;
        internal System.Windows.Forms.ComboBox ComboBox1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox txtmesaj;
        internal System.Windows.Forms.TextBox txtbaslik;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button btnMail;
        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.Button btnYolla;
        internal System.Windows.Forms.Button btnsil;
        internal System.Windows.Forms.Button btnCevap;
    }
}